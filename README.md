## ECE 9309B/9039B: Machine Learning Project

Hira Nadeem (250854119), Ricky Li (251146355), Kyle Jiang (25087791)

Electrical and Computer Engineering
Western University
London, Ontario

Financial engineering is the use of mathematical techniques to solve financial problems. For real practices, the investors and traders may be confused by the complex information on the market. Some sit on the fence in the market and cause idle funds that reduce market efficiency. 

The main objective is to train a neural network model that predicts the occurrence of trade, the action value,  using the [Jane Street Market data](https://www.kaggle.com/c/jane-street-market-prediction/overview). A prediction model based on real market data provides a reliable tool for convenient trading analysis.

An intelligent trader would predict the stock price and buy a stock before the price rises, or sell it before its value declines. Though it is very hard to replace the expertise that an experienced trader has gained, an accurate prediction algorithm can directly result in high profits for investment firms, indicating a direct relationship between the accuracy of the prediction algorithm and the profit made from using the algorithm.